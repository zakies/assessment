({
    myAction : function(component, event, helper) {
        //Create the Fields for the contract
        component.set("v.Columns", [
            {label:"Account Name", fieldName:"AccountName", type:"text"},
            {label:"Contract Number", fieldName:"ContractNumber", type:"Number"},
            {label:"Description", fieldName:"Description", type:"text"},
            {label:"Status", fieldName:"Status", type:"Picklist"},
            {label:"Start Date", fieldName:"StartDate", type:"Date"},
            {label:"End Date", fieldName:"EndDate", type:"Date"}            
        ]);
        
        var action = component.get("c.getContract");
        action.setParams({
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var rows = response.getReturnValue();
            for (var i = 0; i < rows.length; i++) {
                var row = rows[i];
                if (row.Account) row.AccountName = row.Account.Name;
            }
            component.set('v.Contracts', rows);           
        }));
        $A.enqueueAction(action);
    }
	
})