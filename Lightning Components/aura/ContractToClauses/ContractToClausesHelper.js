({
    
    myActionHelper : function(component, event, helper) {
        var action = component.get("c.getClause");
        action.setParams({
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, function(data) {
            component.set("v.ContractClause", data.getReturnValue());
        });
        $A.enqueueAction(action);
    },
       
    getRelatedRecords: function(component, event) {
        var action = component.get('c.getClauses');        
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === 'SUCCESS') {
                component.set('v.RelatedRecordList', actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    addSelectedHelper: function(component, event, relatedRecordsIds) {

        var action = component.get('c.addContract');
        
        action.setParams({
            "ParentId": component.get("v.recordId"),
            "lstOfClausesIds": relatedRecordsIds
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var eve = $A.get("e.force:showToast");
                eve.setParams({
                    "title": "Related List Updated",
                    "message": "The Record's has been added."
                });
                eve.fire();
                                                
                this.getRelatedRecords(component,event);
                
                var sObjectEvent = $A.get("e.force:navigateToSObject");
                sObjectEvent .setParams({
                    "recordId": component.get("v.recordId") ,
                });
                sObjectEvent.fire();
                
            }
        });
        $A.enqueueAction(action);
    },
    
    	removeRecordHelper: function(component,event, helper) {
        
        var action = component.get("c.deleteClause");
        action.setParams({
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var delToast = $A.get("e.force:showToast");
                delToast.setParams({
                    "title": "Related List Updated",
                    "message": "The record has been removed."
                });
                delToast.fire();
                
                var sObjectEvent = $A.get("e.force:navigateToSObject");
                sObjectEvent .setParams({
                    "recordId": component.get("v.recordId") ,
                });
                sObjectEvent.fire();
            }
            
        });
        $A.enqueueAction(action);
    },     
    
})