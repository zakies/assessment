({
    
    openPopup: function(component, event, helper) {
        
        component.set("v.isOpen", true);
    },
    
    closePopup: function(component, event, helper) {
        
        component.set("v.isOpen", false);
    },
    
    myAction : function(component, event, helper) {
        
        helper.myActionHelper(component, event, helper);
    },
    doInit: function(component, event, helper) {
        
        helper.getRelatedRecords(component, event);
    },
    
    addSelected: function(component, event, helper) {
        
        var tempIDs = [];
        
        var getAllId = component.find("checkBox");
        for (var i = 0; i < getAllId.length; i++) {
            
            if (getAllId[i].get("v.value") == true && getAllId[i] != 0) {
                tempIDs.push(getAllId[i].get("v.text"));                       
            }
        }
                helper.addSelectedHelper(component, event, tempIDs);
        		component.set("v.isOpen", false);
    },
    
    RemoveRecord: function(component,event, helper) {
        
        helper.removeRecordHelper(component,event, helper);
    },     
    
    SearchKey: function(component, event, helper) {
        var myEvent = $A.get("e.c:SearchKey");
        myEvent.setParams({"searchKey": event.target.value});
        myEvent.fire();
    },
    searchKeys: function(component, event) {
        var searchKey = event.getParam("searchKey");
        var action = component.get("c.findByName");
        action.setParams({
            "searchKey": searchKey
        });
        action.setCallback(this, function(a) {
            component.set("v.RelatedRecordList", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    
})