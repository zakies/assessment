@isTest
public class ContractControllerTest {
    
    public static testMethod void contractList()
    {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Contract conts = new Contract();
        conts.AccountId= acc.Id;
        conts.Status='Draft';
        conts.StartDate= System.today();
        conts.ContractTerm = 12;
        insert conts;
        
        Test.StartTest();
        ContractController  testCont = new ContractController();
        List<Contract> lstContract = ContractController.getContract(conts.Id);
        System.assert( lstContract  != null);
        Test.stopTest();        
    }
    
    public static testMethod void NegativeTestContract(){
        System.debug('Inserting negative contract');
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;    
        
        Contract conts = new Contract();
        conts.AccountId= acc.Id;
        conts.StartDate= System.today();
        conts.ContractTerm = 12;
        Test.startTest();
        try{
            insert conts; 
        }
        catch (DmlException e) {
            System.assert(e.getMessage().contains('Insert failed.'),
                          e.getMessage());
        }
        Test.stopTest();
    }
}