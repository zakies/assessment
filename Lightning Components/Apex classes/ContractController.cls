public with sharing class ContractController {
@AuraEnabled
    public static List<Contract> getContract(Id recordId)
    {
        return [SELECT Id, Account.Name, ContractNumber, Status, Description, StartDate, EndDate FROM Contract WHERE Id  = :recordId];        
    }
}