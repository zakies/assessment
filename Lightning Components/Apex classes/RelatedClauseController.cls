public with sharing class RelatedClauseController {
@AuraEnabled
    public static list<Contract_to_Clause__c> getClause (Id recordId){      
        return [select Id, Name, Contract__r.ContractNumber, Contract_Clause_Name__r.Name from Contract_to_Clause__c Where Contract__c = :recordId]; 
    }

        @AuraEnabled
    public static List<Contract_Clause__c> getClauses() {    
        return [SELECT id, name, Type__c from Contract_Clause__c];
    }
    
    @AuraEnabled
    public static List<Contract_to_clause__c> getRelatedRecords(id recordId) {
        return [select id, name, Contract__c from Contract_to_clause__c where id = :recordId];
    }
    
    @AuraEnabled
    public static List<Contract_Clause__c> findByName(String searchKey) {
        String name = '%' + searchKey + '%';
        return [SELECT id, name, Type__c FROM Contract_Clause__c WHERE name LIKE :name LIMIT 50];
    }
    
    @AuraEnabled
    public static void addContract(String ParentId, List<String> lstOfClausesIds){
        list<Contract_to_Clause__c> lstClauses = new list<Contract_to_Clause__c>();
        for(string ClausesId : lstOfClausesIds){
            Contract_to_Clause__c pClause = new Contract_to_Clause__c();
            pClause.Contract_Clause_Name__c = ClausesId;
            pClause.Contract__c = ParentId;
            lstClauses.add(pClause);
        }
        if(!lstClauses.isEmpty())
        {
        insert lstClauses;
        }
    }
    
        @AuraEnabled
    public static void deleteClause(id recordId)
    {
            List<Contract_to_Clause__c> del = [select Id, Name, Contract__c from Contract_to_Clause__c Where Contract__c = :recordId];
                delete del;
    }
    
}