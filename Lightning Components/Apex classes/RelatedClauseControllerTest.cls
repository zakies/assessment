@isTest
public class RelatedClauseControllerTest {
    
    private static testMethod void ContractToClause()
    {
        Contract_to_Clause__c cccc = new Contract_to_Clause__c();
        cccc.Name='Clause to Clause Test';
        insert cccc;
        
        Test.StartTest();
        RelatedClauseController  testCCCC = new  RelatedClauseController();
        List<Contract_to_Clause__c> lstContractToClause =  RelatedClauseController.getClause(cccc.Id);
        System.assert( lstContractToClause  != null);
        Test.stopTest();        
    }
    
    private static testMethod void ContractClause()
    {
        Contract_Clause__c ccc = new Contract_Clause__c();
        ccc.Name='Clause Test';
        ccc.Type__c='Financial';
        insert ccc;
        
        Test.StartTest();
        RelatedClauseController  testCCC = new  RelatedClauseController();
        List<Contract_Clause__c> lstContractClause =  RelatedClauseController.getClauses();
        System.assert( lstContractClause  != null);
        Test.stopTest();    
    }
    
    private static testMethod void ContractToClauseRelated()
    {
        Contract_to_Clause__c cccc = new Contract_to_Clause__c();
        cccc.Name='Clause to Clause Test';
        insert cccc;
        
        Test.StartTest();
        RelatedClauseController  testCCCC = new  RelatedClauseController();
        List<Contract_to_Clause__c> lstContractToClause =  RelatedClauseController.getRelatedRecords(cccc.Id);
        System.assert( lstContractToClause  != null);
        Test.stopTest();   
    }
    
    private static testMethod void searchKey()
        
    {
        Contract_Clause__c ccc = new Contract_Clause__c();
        ccc.Name='Clause to Clause Test';
        insert ccc;
        Test.startTest();
        RelatedClauseController.findByName('Clause to Clause Test');
        Test.stopTest();
    }    
    
    private static testMethod void addToContractClauses()
        
    {       
        Contract_Clause__c ccc = new Contract_Clause__c();
        ccc.Name='Clause to Clause Test';
        insert ccc;
        
        account acc = new account();
        acc.Name= 'Test Account';
        insert acc;
        
        contract con = new contract();
        con.AccountId = acc.Id;
        con.Status = 'Draft';
        con.StartDate = System.today();
        con.ContractTerm = 12;
        insert con;
        
        Test.startTest();
        List<string> cccIds = new List<string>{ccc.Id};
            RelatedClauseController.addContract(con.Id, cccIds);
        Test.stopTest();
        
    }
    
    private static testMethod void delContractToClause()
    {
        Contract_to_Clause__c cccc = new Contract_to_Clause__c();
        cccc.Name='Clause to Clause Test';
        insert cccc;
        
        Test.StartTest();        
        RelatedClauseController.deleteClause(cccc.Id);
        Test.stopTest();        
    }
    
}